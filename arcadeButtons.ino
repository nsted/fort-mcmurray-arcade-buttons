/* 
 Based on: 
 http://www.arduino.cc/en/Tutorial/KeyboardMessage
 */

const int debounceDelay = 50;
//const int buttonPins[ 7 ] = { 3, 5, 4, 2, 6, 8, 7 };
const int buttonPins[ 7 ] = { 2, 3, 4, 5, 6, 7, 8 };
int buttonState[ 7 ];
int previousButtonState[ 7 ] = { HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH };
char buttonChars[ 7 ] = { '1', '2', '3', '4', '5', '6', '7' };

void setup() {
  
//  Serial.begin(9600);
//  while (!Serial) {
//    ; // wait for serial port to connect. Needed for Leonardo only
//  }
//  Serial.println("hello");    
  
  // make the pushButton pin an input:
  for( int b = 0; b < 7; b++ ){
    pinMode( buttonPins[ b ], INPUT );
    digitalWrite( buttonPins[ b ], HIGH );
  }
  
  // initialize control over the keyboard:
  Keyboard.begin();
}

void loop() {
  // read the pushbutton:
  
  for( int b = 0; b < 7; b++ ){
    buttonState[ b ] = digitalRead( buttonPins[ b ] );
  
    // if the button state has changed,
    if ( buttonState[ b ] != previousButtonState[ b ] ){
      // and it's currently pressed:
        if ( buttonState[ b ] == LOW ) {
      // type out a message
//      Keyboard.print( b + 1 );
          Keyboard.press( buttonChars[ b ] );
        } else {
//        delay(50);
          Keyboard.release( buttonChars[ b ] );
        }
    }


    // save the current button state for comparison next time:
    previousButtonState[ b ] = buttonState[ b ];
  }
  
  delay(debounceDelay);
}
